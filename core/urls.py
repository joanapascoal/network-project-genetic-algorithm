
from django.conf.urls import include, url
from django.contrib import admin
from rest_framework.documentation import include_docs_urls
from app.viewsets import *

admin.site.site_header = 'Enterprise Network Algorithm'
admin.autodiscover()

API_TITLE = 'API'
API_DESCRIPTION = 'Enterprise Network Algorithm API'

urlpatterns = [
    url(r'^api/', include(router.urls)),
    url(
        r'^api/docs/',
        include_docs_urls(title=API_TITLE, description=API_DESCRIPTION)
    ),
    url(r'^admin/', admin.site.urls),
]
