from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MaxValueValidator, MinValueValidator


class Graph(models.Model):
    name = models.CharField(max_length=100)
    data = models.CharField(max_length=1000000)
    created_on = models.DateTimeField(auto_now_add=True, verbose_name=_('created_on'))
    updated_on = models.DateTimeField(auto_now=True, verbose_name=_('updated_on'))

    def __str__(self):
        return str(self.id)


class Algorithm(models.Model):
    ratio_edges_between_departments = \
        models.FloatField(validators=[MaxValueValidator(1.0), MinValueValidator(0.0)],
                          verbose_name=_(
                              'Ratio of connections between departments (edges) [0.0 - 1.0]'))

    ratio_nodes_between_departments = \
        models.FloatField(validators=[MaxValueValidator(1.0), MinValueValidator(0.0)],
                          verbose_name=_(
                              'Ratio of persons that connect departments (nodes) [0.0 - 1.0]')
                          )

    ratio_edges_on_same_department = \
        models.FloatField(validators=[MaxValueValidator(1.0), MinValueValidator(0.0)], verbose_name=_(
            ' Ratio of edges on same department [0.0 - 1.0]')
                          )

    ratio_edges_on_same_team = \
        models.FloatField(validators=[MaxValueValidator(1.0), MinValueValidator(0.0)],
                          verbose_name=_(
                              'Ratio of edges on same team [0.0 - 1.0]')
                          )

    ratio_people_inside_more_connected_than_average = \
        models.FloatField(validators=[MaxValueValidator(1.0), MinValueValidator(0.0)],
                            verbose_name=_('Ratio of persons that are more connected than average [0.0 - 1.0]')
                          )

    ratio_people_inside_less_connected_than_average = \
        models.FloatField(validators=[MaxValueValidator(1.0), MinValueValidator(0.0)],
                          verbose_name=_('Ratio of persons that are less connected than average [0.0 - 1.0]')
                          )

    created_on = models.DateTimeField(auto_now_add=True, verbose_name=_('created_on'))
    updated_on = models.DateTimeField(auto_now=True, verbose_name=_('updated_on'))

    def __str__(self):
        return str(self.id)
