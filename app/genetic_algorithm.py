import datetime
import random
from random import choice
import operator
from random import randint
import networkx as nx
import numpy

from app.process_variables import ProcessVariables
from app.utils import download_graph, write_output

COMPARATIVE_DATA_PATH = "logs/comparative_data_roleta_without_input_reps=100.txt"
GRAPH_NAME = "graph_roleta_without_input_reps=100"


class GraphGenerate:
    FLAG_USER_INPUT = False
    IS_ELITIST_SELECTION = False
    STOP_CRITERIA = True
    STOP_CRITERIA_VALUE = 0.11

    def __init__(self, graph, iterations=100, graphs_by_iteration=10):
        self.iterations = iterations
        self.graphs_by_iteration = graphs_by_iteration

        self.process_variables = ProcessVariables()

        original_metrics = self.process_variables._graph_data_to_str(graph)
        # "ratio_nodes_between_teams"

        write_output(original_metrics, "logs/compare_metrics.txt")

        self.original_graph = graph
        evaluation = self._evaluate_graph(graph)
        self.graphs_dict = {graph: evaluation}

        self.start_time = datetime.datetime.now()
        print('0:00:00.000000')
        write_output('0:00:00.000000 - ' + str(evaluation), COMPARATIVE_DATA_PATH)

    def generate_graph(self):
        print('----------------------------START-------------------------------')
        write_output('----------------------NEW GENERATION---------------------------------')
        write_output(self.graphs_dict)

        if self.FLAG_USER_INPUT:
            self.iterations = int(self.iterations/2)
            self._run_generation()
            self._get_input()
        else:
            self._run_generation()

        final_graph = operator.itemgetter(0)(self.graphs_dict[0])
        final_score = operator.itemgetter(1)(self.graphs_dict[0])
        download_graph(final_graph, GRAPH_NAME)
        write_output(final_graph)

        final_time = datetime.datetime.now() - self.start_time
        write_output(str(final_time) + ' - ' + str(final_score), COMPARATIVE_DATA_PATH)
        print(str(final_time))

        final_metrics = self.process_variables._graph_data_to_str(final_graph)

        write_output(final_metrics, "logs/compare_metrics.txt")
        print('----------------------------END-------------------------------')

        return final_graph

    def _run_generation(self):
        self._run_first_round()

        self._filter_to_next_iteration()

        write_output(self.graphs_dict)

        if self.STOP_CRITERIA:
            while operator.itemgetter(1)(self.graphs_dict[0]) > self.STOP_CRITERIA_VALUE:
                self._run_second_round()
                self._filter_to_next_iteration()
                write_output(self.graphs_dict)
        else:
            for i in range(self.iterations - 1):
                self._run_second_round()
                self._filter_to_next_iteration()
                write_output(self.graphs_dict)

    def _run_first_round(self):
        graphs_for_round = 99
        while graphs_for_round != 0:
            copy_graph = self.original_graph.copy()
            random_range = randint(1, 5)
            copy_graph = self._remove_random_edge(copy_graph, random_range)
            copy_graph = self._add_random_edge(copy_graph, random_range)
            copy_graph = self._correct_isolated_edges(copy_graph)
            evaluation = self._evaluate_graph(copy_graph)
            self.graphs_dict.update({copy_graph: evaluation})
            graphs_for_round -= 1

    def _run_second_round(self):
        graphs_for_round = self.graphs_by_iteration
        temp = self.graphs_dict.copy()
        self.graphs_dict = {}
        for graph, _ in temp:
            while graphs_for_round != 0:
                copy_graph = graph.copy()
                random_range = randint(0, 2)
                copy_graph = self._remove_random_edge(copy_graph, random_range)
                copy_graph = self._add_random_edge(copy_graph, random_range)
                copy_graph = self._correct_isolated_edges(copy_graph)
                evaluation = self._evaluate_graph(copy_graph)
                self.graphs_dict.update({copy_graph: evaluation})
                graphs_for_round -= 1
            graphs_for_round = self.graphs_by_iteration

    def _remove_random_edge(self, copy_graph, random_range: int):
        for i in range(random_range):
            random_position = random.randrange(0, len(copy_graph.edges()))
            random_edge = tuple(copy_graph.edges())[random_position]
            copy_graph.remove_edge(random_edge[0], random_edge[1])
        return copy_graph

    def _add_random_edge(self, copy_graph, random_range: int):
        for i in range(random_range):
            random_node_1 = choice(list(copy_graph.nodes()))
            random_node_2 = choice(list(copy_graph.nodes()))
            copy_graph.add_edge(random_node_1, random_node_2)
        return copy_graph

    def _correct_isolated_edges(self, copy_graph):
        while len(list(nx.isolates(copy_graph))) > 0:

            node = list(nx.isolates(copy_graph))[0]
            new_list = [x for x in list(copy_graph.nodes())
                        if x not in list(nx.isolates(copy_graph))]
            choosen_node = choice(new_list)

            if len(list(copy_graph.neighbors(choosen_node))) > 5 : copy_graph.add_edge(choosen_node, node)

        new_list = [x for x in list(copy_graph.nodes())
                    if x not in list(nx.isolates(copy_graph))]
        choosen_node = choice(new_list)

        [copy_graph.add_edge(choosen_node, node) for node in copy_graph.nodes()
            if len(list(copy_graph.neighbors(node))) < 3 ]

        return copy_graph

    def _evaluate_graph(self, graph):
        return self.process_variables.process_algorithm_vars(graph)

    def _filter_to_next_iteration(self):
        if self.IS_ELITIST_SELECTION:
            self._filter_top_ten()
        else:
            self._select_ten_with_probability()

    # User Input if flag_user_input=True
    def _get_input(self):
        write_output('----------------------USER INPUT---------------------------------')
        # User input
        self.graphs_dict = self.graphs_dict[:3]
        i = 0
        for graph in self.graphs_dict:
            download_graph(graph[0], str(i))
            write_output('graph ' + str(i))
            write_output(nx.nodes(graph[0]))
            write_output(nx.edges(graph[0]))
            i += 1

        user_input = input('Please choose one of the graphs: \n 0, 1 or 2 \n -> Your choice:  ')
        write_output('USER INPUT ' + user_input)

        # Apply user option - new generation
        self.graphs_dict = {
            self.graphs_dict[int(user_input)][0]: self.graphs_dict[int(user_input)][1]
        }
        write_output(self.graphs_dict)
        self._run_generation()

    # Graph Selection if IS_ELITIST_SELECTION=True
    def _filter_top_ten(self):
        # Select only the 10 graphs with best fit (close to 0)
        self.graphs_dict = sorted(self.graphs_dict.items(), key=operator.itemgetter(1))
        self.graphs_dict = self.graphs_dict[:10]

        diff_time = datetime.datetime.now() - self.start_time
        write_output(str(diff_time) + " - " + str(operator.itemgetter(1)(self.graphs_dict[0])),
                     COMPARATIVE_DATA_PATH)
        print(str(diff_time))

    # Graph Selection if IS_ELITIST_SELECTION=False

    def _select_ten_with_probability(self):

        list_prob = []
        list_evaluation = []
        for graph, evaluation in self.graphs_dict.items():
            list_evaluation.append(evaluation)
            list_prob.append(evaluation / sum(self.graphs_dict.values()))

        # Invert probability
        list_prob = [(1 - p) for p in list_prob]
        list_prob = [p/sum(list_prob) for p in list_prob]

        self.graphs_dict = sorted(self.graphs_dict.items(), key=operator.itemgetter(1))
        list_graphs = [graph for graph, prob in self.graphs_dict]

        indexes = numpy.random.choice(len(list_graphs), 10, p=list_prob, replace=False)

        self.graphs_dict = [self.graphs_dict[index] for index in indexes]

        graph_evaluations = [list_evaluation[index] for index in indexes]

        diff_time = datetime.datetime.now() - self.start_time
        write_output(str(diff_time) + " - " + str(sorted(graph_evaluations)[0]),
                 COMPARATIVE_DATA_PATH)
