from ast import literal_eval

from rest_framework import serializers
from networkx.readwrite import json_graph

from app.models import Graph, Algorithm
from .graph import *
from .genetic_algorithm import *


class GraphSerializer(serializers.ModelSerializer):
    class Meta:
        model = Graph
        fields = ('id', 'name', 'data', 'created_on', 'updated_on')
        read_only_fields = ('data',)

    def create(self, data):
        nodes = import_nodes_file()
        edges = import_edge_file()
        node_names = [n[0] for n in nodes]  # Get a list of only the node names

        G = nx.Graph()

        G.add_nodes_from(node_names)
        G.add_edges_from(edges)

        save_atributes(G, nodes)

        isolate_nodes = list(nx.isolates(G))

        for node in isolate_nodes:
            G.remove_node(node)

        data['data'] = json_graph.node_link_data(G)
        return super(GraphSerializer, self).create(data)

    def download_graph(self):
        data_graph = json_graph.node_link_graph(literal_eval(self.instance.data))
        path = os.path.join(os.path.join('data', 'outputs'), self.instance.name + '.gexf')
        nx.write_gexf(data_graph, path)
        
        return data_graph

    def generate_graph(self):
        data_graph = json_graph.node_link_graph(literal_eval(self.instance.data))
        graph_generator = GraphGenerate(data_graph)
        return graph_generator.generate_graph()


class AlgorithmSerializer(serializers.ModelSerializer):
    class Meta:
        model = Algorithm
        fields = ('id', 'ratio_edges_between_departments', 'ratio_nodes_between_departments',
                  'ratio_edges_on_same_department',
                  'ratio_edges_on_same_team',
                  'ratio_people_inside_more_connected_than_average',
                  'ratio_people_inside_less_connected_than_average',
                  'created_on', 'updated_on')
