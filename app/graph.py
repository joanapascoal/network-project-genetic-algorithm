import csv
import os
import networkx as nx

NODES_FILE = 'dataset_nodes.csv'
EDGES_FILE = 'dataset_edges_leader.csv'

def import_nodes_file():
    with open(os.path.join('data', NODES_FILE), 'r') as nodecsv:  # Open the file
        nodereader = csv.reader(nodecsv)  # Read the csv
        # Retrieve the data (using Python list comprehension and list slicing to remove the header
        # row, see footnote 3)
        nodes = [n for n in nodereader][1:]
    return nodes


def import_edge_file():
    with open(os.path.join('data', EDGES_FILE), 'r') as edgecsv:  # Open the file
        edgereader = csv.reader(edgecsv)  # Read the csv
        edges = [tuple(e) for e in edgereader][1:]  # Retrieve the data
    return edges


def save_atributes(G, nodes):
    # Create an empty dictionary for each attribute
    depart_dict = {}
    team_dict = {}
    academic_dict = {}
    year_in_comp_dict = {}

    for node in nodes:  # Loop through the list of nodes, one row at a time
        #  Access the correct item, add it to the corresponding dictionary
        depart_dict[node[0]] = node[1]
        team_dict[node[0]] = node[2]
        academic_dict[node[0]] = node[3]
        year_in_comp_dict[node[0]] = node[4]

    # Add each dictionary as a node attribute to the Graph object
    nx.set_node_attributes(G, depart_dict, 'department')
    nx.set_node_attributes(G, team_dict, 'team')
    nx.set_node_attributes(G, academic_dict, 'academic_degree')
    nx.set_node_attributes(G, year_in_comp_dict, 'years_in_company')
