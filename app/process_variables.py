import math

from app.models import Algorithm
from app.utils import normalize_data, write_output


class ProcessVariables:
    def __init__(self):
        algorithm = Algorithm.objects.latest('created_on')
        self.ratio_edges_between_departments = algorithm.ratio_edges_between_departments
        self.ratio_nodes_between_departments = algorithm.ratio_nodes_between_departments
        self.ratio_edges_on_same_department = \
            algorithm.ratio_edges_on_same_department
        self.ratio_edges_on_same_team = \
            algorithm.ratio_edges_on_same_team
        self.ratio_people_inside_more_connected_than_average = \
            algorithm.ratio_people_inside_more_connected_than_average
        self.ratio_people_inside_less_connected_than_average = \
            algorithm.ratio_people_inside_less_connected_than_average

        algorithm_goal = self._graph_data_to_str(None, True)

        write_output(algorithm_goal, "logs/compare_metrics.txt")

    def process_algorithm_vars(self, graph) -> float:
        sum = math.fabs(self.ratio_edges_between_departments - self._get_ratio_edges_between_departments(graph)) + \
                math.fabs(self.ratio_nodes_between_departments - self._get_ratio_nodes_between_departments(graph)) + \
                math.fabs(self.ratio_edges_on_same_department - self._get_ratio_edges_on_same_department(graph)) + \
                math.fabs(self.ratio_edges_on_same_team - self._get_ratio_edges_on_same_team(graph))
        normalized_value = normalize_data(sum, 0.0, 4.0)

        return normalized_value

    def _get_ratio_edges_between_departments(self, graph) -> float:
        # ratio of edges between different departments
        # (departs/(egdes between != departs)) [0.00-1.00]

        departments = {}

        for (p, d) in graph.nodes(data=True):
            if d['department'] in departments:
                departments[d['department']] += 1
            else:
                departments[d['department']] = 1

        # get from graph
        edges_on_extra_department = [(n, u) for n, u in graph.edges()
                                     if (
                                         graph.nodes[n]['department'] != graph.nodes[u][
                                         'department']
                                     )
                                     ]

        average_of_persons_by_department = sum(departments.values()) / len(departments.items())

        average_of_max_edges_between_department = (len(
            graph.nodes()) - average_of_persons_by_department) * average_of_persons_by_department

        real_ratio_edges_between_departments = len(edges_on_extra_department) / len(
            departments) / average_of_max_edges_between_department

        return real_ratio_edges_between_departments

    def _get_ratio_nodes_between_departments(self, graph) -> float:
        # ratio of persons that have neighbors extra departament
        # (people_connected_extra_department/total_of_people)

        nodes_found = list(set([n for n in graph.nodes
                                for neighbor in graph.neighbors(n)
                                if (
                                    graph.nodes[n]['department'] != graph.nodes[neighbor][
                                    'department']
                                )]))

        ratio_of_people_connected_extra_depart = len(nodes_found) / len(graph.nodes())

        return ratio_of_people_connected_extra_depart

    def _get_ratio_edges_on_same_department(self, graph) -> float:
        # Calculate the total number of edges connected inside department and split by number of
        # departments. Then, split for the average_of_max_edges_inside_a_department [0.0 - 1.0]

        departments = {}

        for (p, d) in graph.nodes(data=True):
            if d['department'] in departments:
                departments[d['department']] += 1
            else:
                departments[d['department']] = 1

        # get from graph
        edges_on_same_department = [(n, u) for n, u in graph.edges()
                                     if (
                                         graph.nodes[n]['department'] == graph.nodes[u][
                                         'department']
                                     )
                                     ]

        average_of_persons_by_department = sum(departments.values()) / len(
            departments.items())

        average_of_max_edges_on_same_department = (len(
            graph.nodes()) - average_of_persons_by_department) * \
                                                  average_of_persons_by_department

        real_ratio_edges_on_same_department = len(edges_on_same_department) / len(
            departments) / average_of_max_edges_on_same_department

        return real_ratio_edges_on_same_department

    def _get_ratio_edges_on_same_team(self, graph) -> float:
        # Calculate the total number of edges connected inside a team and split by number of
        # teams. Then, split for the average_of_max_edges_inside_a_department [0.0 - 1.0]

        teams = {}

        for (p, d) in graph.nodes(data=True):
            if d['team'] in teams:
                teams[d['team']] += 1
            else:
                teams[d['team']] = 1

        # get from graph
        edges_on_same_department = [(n, u) for n, u in graph.edges()
                                    if (
                                        graph.nodes[n]['team'] == graph.nodes[u][
                                        'team']
                                    )
                                    ]

        average_of_persons_by_team = sum(teams.values()) / len(
            teams.items())

        average_of_max_edges_on_same_team = (len(
            graph.nodes()) - average_of_persons_by_team) * \
                                                  average_of_persons_by_team

        real_ratio_edges_on_same_team = len(edges_on_same_department) / len(
            teams) / average_of_max_edges_on_same_team

        return real_ratio_edges_on_same_team

    def _get_ratio_people_inside_connected_more_than_average(self, graph):
        # TODO: Implement variable process
        pass

    def _get_ratio_people_inside_connected_less_than_average(self, graph):
        # TODO: Implement variable process
        pass

    def _graph_data_to_str(self, graph=None, algorithm=False) -> str:
        if algorithm:
            return "Algorithm:\n" \
                "ratio_edges_between_departments : " + \
                    str(self.ratio_edges_between_departments) + "\n" \
                "ratio_nodes_between_departments : " + \
                    str(self.ratio_nodes_between_departments) + "\n" \
                "ratio_edges_on_same_department : " + \
                    str(self.ratio_edges_on_same_department) + "\n" \
                "ratio_edges_on_same_team : " + str(
                    self.ratio_edges_on_same_team) + "\n" \
                "ratio_people_inside_more_connected_than_average : " + \
                    str(self.ratio_people_inside_more_connected_than_average) + "\n" \
                "ratio_people_inside_less_connected_than_average : " + \
                       str(self.ratio_people_inside_less_connected_than_average) + "\n\n"

        return "ratio_edges_between_departments : " + \
            str(self._get_ratio_edges_between_departments(graph)) + "\n" \
        "ratio_nodes_between_departments : " + \
            str(self._get_ratio_nodes_between_departments(graph)) + "\n" \
        "ratio_edges_on_same_department : " + \
               str(self._get_ratio_edges_on_same_department(graph)) + "\n" \
        "ratio_edges_on_same_team : " +str(self._get_ratio_edges_on_same_team(graph)) + "\n\n" \
        # "ratio_people_inside_more_connected_than_average : " + \
        #     str(self._get_ratio_people_inside_connected_more_than_average) + "\n " \
        # "ratio_people_inside_less_connected_than_average : " + \
        #        str(self._get_ratio_people_inside_connected_less_than_average)

