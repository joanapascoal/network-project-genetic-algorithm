import os

import networkx as nx


def normalize_data(data, min, max) -> float:
    return (data - min) / (max - min)


def download_graph(graph, name):
    path = os.path.join(os.path.join('data', 'outputs'), name + '.gexf')
    nx.write_gexf(graph, path)


def write_output(data, path="logs/output.txt"):
    print(data, file=open(path, "a"))