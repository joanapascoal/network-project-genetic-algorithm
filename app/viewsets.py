# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.decorators import detail_route
from rest_framework.viewsets import GenericViewSet
from rest_framework import mixins, status

from .models import Graph, Algorithm
from .serializers import GraphSerializer, AlgorithmSerializer
from core import router


class GraphViewSet(
        mixins.CreateModelMixin, mixins.ListModelMixin,
        mixins.RetrieveModelMixin, GenericViewSet
):
    """
        Network graph management

        create:
        Creates an graph from csv.

        list:
        Return a list of all the existing graphs.

        update:
        Search and update a graph information by Id.

        delete:
        Remove a graph of Database.

    """
    serializer_class = GraphSerializer
    queryset = Graph.objects.all()

    @detail_route(
            methods=['get'])
    def download_graph(self, request, pk=None):
        """
         Search and return a graph draw
        """
        graph = get_object_or_404(Graph, pk=pk)
        serializer = GraphSerializer(graph)
        serializer.download_graph()

        return Response(
                {
                    "data": serializer.data['data']
                }
        )

    @detail_route(
            methods=['get'])
    def generate_graph(self, request, pk=None):
        """
        Generate a new graph from latest algorithm
        """
        graph = get_object_or_404(Graph, pk=pk)
        serializer = GraphSerializer(graph)
        serializer.generate_graph()

        return Response(
                {
                    "data": serializer.data['data']
                }
        )


class AlgorithmViewSet(
        mixins.CreateModelMixin, mixins.RetrieveModelMixin, mixins.ListModelMixin,
        mixins.UpdateModelMixin,
        GenericViewSet
):
    """
        Algorithm manipulation

        create:
        Creates a new algorithm

        list:
        Return a list of all algorithms

        read:
        Search and return a algorithm info by Id

        update:
        Search and update an existing algorithm

        delete:
        Remove a algorithm by id

    """
    serializer_class = AlgorithmSerializer
    queryset = Algorithm.objects.all()


router.register(r'graph', GraphViewSet)
router.register(r'algorithm', AlgorithmViewSet)