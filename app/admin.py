from django.contrib import admin

from .models import Graph, Algorithm


class GraphAdmin(admin.ModelAdmin):
    list_display = [
        'id', 'name', 'created_on', 'updated_on'
    ]
    list_filter = ['name', 'created_on', 'updated_on']
    search_fields = ['id', 'name']
    readonly_fields = [

    ]


class AlgorithmAdmin(admin.ModelAdmin):
    list_display = [
        'id', 'created_on', 'updated_on'
    ]
    list_filter = ['created_on', 'updated_on']
    search_fields = ['id']
    readonly_fields = [
        'created_on', 'updated_on'
    ]


admin.site.register(Graph, GraphAdmin)
admin.site.register(Algorithm, AlgorithmAdmin)