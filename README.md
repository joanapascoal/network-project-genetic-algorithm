# Enterprise Network Optimization

This project is a prototype that applies a genetic algorithm to restructure and 
reorganize an enterprise network.

### Running

```
pipenv install

pipenv shell

python3 manage.py runserver
```

## Endpoints

After app is running: 
```
http://127.0.0.1:8000/api/docs/
```

## Running the tests

```
python3 manage.py tests
```

## Built With

* [Networkx](https://networkx.github.io/)
* [Django Rest Framework](http://www.django-rest-framework.org/)


## Authors

* **Joana Pascoal** - *Initial work* - [GitHub](https://github.com/joanapascoal) [Linkedin](https://www.linkedin.com/in/joanapascoal/)
